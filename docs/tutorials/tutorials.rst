Immcantation Tutorials
===========================================================================================

Each tool in the framework has its own documentation site, with detailed usage information 
and examples. A good starting point to familiarize yourself with the framework is to
follow one the tutorials listed here.


Introductory Webinar and Jupyter Notebook
-------------------------------------------------------------------------------------------

For a detailed use example for each Immcantation tool see the
`slides and example data <https://goo.gl/FpW3Sc>`_ from our introductory webinar series. 
You can find a Jupyter notebook version of the webinar `in the repository <https://bitbucket.org/kleinstein/immcantation/src/master/training/>`_.
If you don't want to execute the Jupyter notebook yourself, you can explore a
`website version of it here <https://kleinstein.bitbucket.io/tutorials/intro-lab/index.html>`_.


This webinar covers:

* V(D)J gene annotation and novel polymorphism detection

* Inference of B cell clonal relationships

* Diversity analysis

* Mutational load profiling

* Modeling of somatic hypermutation (SHM) targeting

* Quantification of selection pressure


Single-cell Analysis
-------------------------------------------------------------------------------------------

For information on how to process 10x Genomics data to be analyzed with Immcantation, we offer an
introductory tutorial for new users, and a Change-O reference page for users more familiar
with Immcantation:

.. toctree::
    :maxdepth: 1

    10x_tutorial
    Parsing 10x Genomics data with Change-O <https://changeo.readthedocs.io/en/stable/examples/10x.html>
