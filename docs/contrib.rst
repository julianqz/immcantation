.. _Contributing:

Contributing
================================================================================

We welcome contributions to all components of the Immcantation framework through
pull request to the relevant Bitbucket repository:

+ `Docker container, pipelines and portal documentation <https://bitbucket.org/kleinstein/immcantation>`__
+ `pRESTO <https://bitbucket.org/kleinstein/presto>`__
+ `Change-O <https://bitbucket.org/kleinstein/changeo>`__
+ `Alakazam <https://bitbucket.org/kleinstein/alakazam>`__
+ `SHazaM <https://bitbucket.org/kleinstein/shazam>`__
+ `TIgGER <https://bitbucket.org/kleinstein/tigger>`__
+ `SCOPer <https://bitbucket.org/kleinstein/scoper>`__

All Immcantation core software packages are under the free and open-source license
`AGPL-3 <https://www.gnu.org/licenses/agpl-3.0.html>`__. Other core elements, including, 
but not limited to, documentation and tutorials, are 
under `CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>`__. Contributed
packages are subject to their licenses.

For details on documentation, coding style, and other conventions see the
`CONTRIBUTING.md <https://bitbucket.org/kleinstein/immcantation/src/master/CONTRIBUTING.md>`__ file on
Bitbucket.
